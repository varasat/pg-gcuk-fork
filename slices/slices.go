package main

import "fmt"

func main() {
	cart := []string{
		"lemon",
		"apple",
		"banana",
	}

	for i := range cart {
		fmt.Print(i, " ")
	}

	fmt.Println()
	fmt.Println()

	for i, v := range cart {
		fmt.Println("position: ", i, " value: ", v)
	}

	fmt.Println()

	for _, v := range cart {
		fmt.Print(v, " ")
	}

	fmt.Println()
	fmt.Println()

	cart = append(cart, "bread")
	fruit := cart[:3] //slicing, half open range
	fmt.Println("cart:", cart)
	fmt.Println("fruit:", fruit)
	var values []int
	for i := 0; i < 1000; i++ {
		values = appendInt(values, i)
	}
	fmt.Println(values[:10])
}

func appendInt(valueArray []int, val int) []int {
	i := len(valueArray)
	if len(valueArray) < cap(valueArray) { //enough space in underlying array
		valueArray = valueArray[:len(valueArray)+1]
	} else {
		size := 2 * (len(valueArray) + 1)
		fmt.Print("Cap:")
		fmt.Println(cap(valueArray) + 1)
		s := make([]int, size)
		copy(s, valueArray)
		valueArray = s[:len(valueArray)+1]
	}

	valueArray[i] = val
	return valueArray
}
