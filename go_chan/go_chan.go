package main

import (
	"fmt"
	"time"
)

//concurrency exercise stuff
func main() {
	go fmt.Println("goroutine") //-> it's a routine, once you start it you can't change it
	fmt.Println("main")
	for i := 0; i < 5; i++ {
		//BUG: ALL GOROUTINES USE THE SAME i FROM LINE 12
		//go func() {
		//	fmt.Println("goroute:", i)
		//}()
		//FIX 1
		go func(n int) {
			fmt.Println("goroute:", n)
		}(i)
		//FIX 2
		//go fmt.Println("goroute:", i)
		//FIX 3
		//i:= i
		//go func() {
		//	fmt.Println("goroute:", i)
		//}()
	}
	time.Sleep(10 * time.Millisecond)

	//CHANNELS
	ch := make(chan string)
	go func() {
		ch <- "hi" //send
	}()

	val := <-ch //receive
	fmt.Println(val)

	fmt.Println(sleepSort([]int{30, 10, 20})) // [10 20 30]
	go func() {                               //producer: makes stuff that happens
		defer close(ch)
		for i := 0; i < 3; i++ {
			ch <- fmt.Sprintf("message #%d", i)
		}
		//close(ch) //close the channel if you're forlooping through it afterwards so you don't have asleep routines
	}()
	for msg := range ch { //get the values one after another
		fmt.Println(msg)
	}
	msg := <-ch
	fmt.Println("msg: ", msg) //should return an empty string (or the zero value of a type)
	msg, ok := <-ch           // same as above but allows us to know if channel is closed
	if !ok {
		fmt.Println("CHANNEL IS CLOSED")
	}
	//sending/closing an already closed channel will PANIC
	//QUEUE
	queue := make(chan int, 1)
	queue <- 7
	fmt.Println("sent to queue")
	//buffer tells the channel that it should buffer some values without deadlocking
	//but doesn't guarantee the proof of delivery
}

/*
for every value "n" in values, spin a goroutine that
  - sleep n milliseconds
  - send n over a channel

collect all values from the channel to a slice and return it
*/
func sleepSort(values []int) []int {
	// FIXME
	// Hint: convert int to a float:
	// n := 1
	// f := float64(n)
	ch := make(chan int)
	//fan out the routines
	for _, n := range values {
		n := n
		go func() {
			time.Sleep(time.Duration(n) * time.Millisecond)
			ch <- n
		}()
	}
	//collect from channels
	var sorted []int
	for range values {
		val := <-ch
		sorted = append(sorted, val)
	}

	return sorted
}
