package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	url2 "net/url"
	"os"
	"time"
)

/*
Working with JSON in Go
encoding/json API
JSON -> io.Reader -> Go: json.NewDecoder
Go -> io.Writer -> JSON: json.NewEncoder
JSON -> []bite -> Go:json.Unmarshal
Go -> [byte -> JSON: json.Marshal
*/

type UserInfo struct {
	Name        string `json:"name"`
	PublicRepos int    `json:"public_repos"`
}

const (
	urlForGithub = "https://api.github.com/users/"
)

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	usrInfo1, err := getUserInfo(ctx, "ardenLabs")

	if err != nil {
		fmt.Println(err.Error())
	}
	if usrInfo1 != nil {
		fmt.Println(usrInfo1.Name)
		fmt.Println(usrInfo1.PublicRepos)
	}
	usrInfo, err := ReturnDataFromFile()
	if err != nil {
		log.Fatalf("FAILED TO RETURN", err)
	}

	if usrInfo != nil {
		fmt.Println(usrInfo)
		return
	}

	usrInfo2, err := getUserInfo(ctx, "ardenlabs")
	if err != nil {
		log.Fatalf("ERROR in getting from the api")
	}
	if usrInfo2 != nil {
		fmt.Println(usrInfo2)
	}
}

//		Solve: Get Parse Analyze Output
func getUserInfo(ctx context.Context, userName string) (*UserInfo, error) {
	url := urlForGithub + url2.PathEscape(userName)
	//resp, err := http.Get(url)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("Wrong status code")
	}

	return decodeUserInfo(resp.Body), nil
}

func decodeUserInfo(body io.Reader) *UserInfo {
	dec := json.NewDecoder(body)
	var userInfo UserInfo
	if err := dec.Decode(&userInfo); err != nil {
		log.Fatalf("cant decode JSON")
	}
	return &userInfo
}

func ReturnDataFromFile() (*UserInfo, error) {
	file, err := os.Open("github/reply.json")

	if err != nil {
		fmt.Println("FAILED TO FIND THE FILE")
		return nil, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {

		}
	}(file)

	userInfo := decodeUserInfo(file)
	return userInfo, err
}
