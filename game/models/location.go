package models

type Location struct {
	X int
	Y int
}

// l is called the receiver
func (l *Location) Move(x, y int) {
	l.X = x
	l.Y = y
}
