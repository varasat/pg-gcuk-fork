package models

//interface
type Mover interface {
	Move(int, int)
}

func MoveAll(ms []Mover, x, y int) {
	for _, m := range ms {
		m.Move(x, y)
	}
}
