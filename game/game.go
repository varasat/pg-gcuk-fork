package main

import (
	"errors"
	"fmt"
	"log"
	"practical-go/game/models"
)

const (
	maxX = 1000
	maxY = 600
)

func NewLocation(x, y int) (*models.Location, error) {
	if x > maxX {
		return nil, errors.New("X too high")
	}

	if x < 0 {
		return nil, errors.New("X too low")
	}

	if y > maxY {
		return nil, errors.New("Y too high")
	}

	if y < 0 {
		return nil, errors.New("Y too low")
	}

	loc := models.Location{X: x, Y: y}
	return &loc, nil
}

func main() {
	//var loc models.Location
	//loc.y = 2
	//loc.x = 1
	//loc := models.Location{x: 1, y: 2}
	//loc := models.Location{1, 2}
	loc, err := NewLocation(1, 299) // just mess around with the values then rerun, you'll be able to catch the errors
	if err != nil {
		log.Fatalf(err.Error())
	}
	printLocationValues(*loc)
	loc.Move(200, 200)
	printLocationValues(*loc)
	p1 := models.Player{
		Name:     "Andy",
		Location: *loc,
	}
	p1.Move(1, 1)
	printLocationValues(p1.Location)

	ms := []models.Mover{
		loc,
		&p1,
	}
	models.MoveAll(ms, 356, 255)
	printLocationValues(p1.Location)
	printLocationValues(*loc)
}

func printLocationValues(loc models.Location) {
	fmt.Printf("loc (v): %v\n", loc)   //just values
	fmt.Printf("loc (+v): %+v\n", loc) //name and values
	fmt.Printf("loc (#v): %#v\n", loc) //struct name, name then values
}
