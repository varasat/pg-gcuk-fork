package main

import (
	"fmt"
	"unicode/utf8"
)

func banner(text string, width int) {
	//old without taking into account rune length
	//padding := (width - len(text)) / 2

	//better
	padding := (width - utf8.RuneCountInString(text)) / 2
	for i := 0; i < padding; i++ {
		fmt.Print(" ")
	}

	fmt.Println(text)

	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
	fmt.Println()
	fmt.Println()
}

func main() {

	banner("GO", 6)
	banner("G😀", 6)

	//go through the string treated as runes and see how many bites everything occupies
	s := "G😀"
	fmt.Println("rune length")
	for i := range s {
		fmt.Print(i, " ")
	}
	fmt.Println()
	fmt.Println()
	fmt.Println("Rune value and rune itself")
	for i, c := range s {
		fmt.Printf("%d, %c", i, c)
	}
	fmt.Println()

	a := "1"
	//gets the value
	fmt.Printf("a=%v", a)
	//gets the value as shown in code with the #
	fmt.Printf("a=%#v", a)
	//"" -> string that takes into account joker characters
	//`` -> string that is RAW
}
